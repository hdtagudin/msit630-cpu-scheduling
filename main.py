import copy
import sys
from os import system, name
import schedulers

# define our clear function
def clear():
    # for windows
    if name == 'nt':
        _ = system('cls')

    # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')


print("=================================================================================================")
print("                                       CPU SCHEDULING                                            ")
print("=================================================================================================")

print("Select algorithm to use: ")
print("1 : First Come First Serve (FCFS)")
print("2 : Shortest Job First (SJF)")
print("3 : Shortest Remaining Time First (SRTF)")
print("4 : Non-Preemptive Priority (NPP)")
print("5 : Preemptive Priority (PP)")
print("6 : Round Robin (RR)")

algorithmChoice = int(input("Enter number of choice [1-6] : "))
if algorithmChoice < 1 or algorithmChoice > 6:
    print("Invalid choice. Please run the application again.")
    sys.exit(1)

clear()

processesCt = int(input("Enter number of processes [1-26] : "))
if processesCt < 1 or processesCt > 26:
    print("Invalid process range [1-26]. Please run the application again.")
    sys.exit(2)

d = dict()
q = 0

for i in range(processesCt):
    key = str(chr(65 + i))
    a = int(input("Enter arrival time of process " + key + ": "))
    b = int(input("Enter burst time of process " + key + ": "))
    if 3 < algorithmChoice < 6:
        c = int(input("Enter priority of process " + key + ": "))
    else:
        c = 0
    ct = copy.deepcopy(a)
    process = [a, b, c, ct]
    d[key] = process

if algorithmChoice == 6:
    q = int(input("Enter RR quantum: "))

if algorithmChoice == 1:
    schedulers.fcfs.process(d, processesCt)
elif algorithmChoice == 2:
    schedulers.sjf.process(d, processesCt)
elif algorithmChoice == 3:
    schedulers.srtf.process(d, processesCt)
elif algorithmChoice == 4:
    schedulers.npp.process(d, processesCt)
elif algorithmChoice == 5:
    schedulers.pp.process(d, processesCt)
elif algorithmChoice == 6:
    schedulers.rr.process(d, processesCt, q)
