from .fcfs import FCFSAlgorithm
from .npp import NPPAlgorithm
from .pp import PPAlgorithm
from .rr import RRAlgorithm
from .sjf import SJFAlgorithm
from .srtf import SRTFAlgorithm

