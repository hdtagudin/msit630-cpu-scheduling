def process(d, n):
    d = sorted(d.items(), key=lambda item: (item[1][0]))
    start_times = []
    end_times = []
    for i in range(len(d)):
        # first process
        if i == 0:
            start_times.append(d[i][1][0])
            end_times.append(d[i][1][0] + d[i][1][1])

        # get prevET + newBT
        else:
            start = end_times[i - 1]
            if d[i][1][0] > end_times[i - 1]:
                start = d[i][1][0]
            start_times.append(start)
            end_times.append(start + d[i][1][1])

    turnaround_times = []
    for i in range(len(d)):
        turnaround_times.append(end_times[i] - d[i][1][0])

    waiting_times = []
    for i in range(len(d)):
        waiting_times.append(turnaround_times[i] - d[i][1][1])

    avg_wt = 0
    for i in waiting_times:
        avg_wt += i
    avg_wt = (avg_wt / n)

    avg_tat = 0
    for i in turnaround_times:
        avg_tat += i
    avg_tat = (avg_tat / n)
    print("=================================================================================================")
    print("                                      FCFS SCHEDULING                                            ")
    print("=================================================================================================")
    print("=================================================================================================")
    print("Process     |  Arrival  |   Burst  |   Start   |      End     | Turnaround (TAT) | Waiting (WT) |")
    print("=================================================================================================")

    for i in range(n):
        print("   ",
              str(d[i][0]).rjust(4), "   |    ",
              str(d[i][1][0]).rjust(4), " |   ",
              str(d[i][1][1]).rjust(4), " |    ",
              str(start_times[i]).rjust(4), " |       ",
              str(end_times[i]).rjust(4), " |       ",
              str(turnaround_times[i]).rjust(4), "     |   ",
              str(waiting_times[i]).rjust(4), "     |  ")
        print("---------------------------------------------------------------------------------------------")

    print("   ", "Ave.", "   |           |          |           |              |     ", str(round(avg_tat, 4)).rjust(8),
          "   | ", str(round(avg_wt, 4)).rjust(8), "   |  ")
    print("=================================================================================================")


class FCFSAlgorithm:
    pass
