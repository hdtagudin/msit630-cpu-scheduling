def process(d, n):
    d = sorted(d.items(), key=lambda item: (item[1][0], item[1][1]))
    start_times = []
    end_times = []

    job_queue = []
    curr_start_time = d[0][1][0]
    i = 0

    while len(d) > 0:
        filtered = filter(lambda item: curr_start_time >= item[1][0], d)
        filtered = sorted(filtered, key=lambda item: (item[1][1]))
        job = filtered.pop(0)
        job_start = job[1][0]

        if i == 0:
            curr_start_time = job[1][0] + job[1][1]

        else:
            job_start = end_times[i - 1]
            if job[1][0] > end_times[i - 1]:
                job_start = job[1][0]
            curr_start_time = curr_start_time + job[1][1]
        start_times.append(job_start)
        end_times.append(job_start + job[1][1])

        job_queue.append(job)
        d.remove(job)
        i += 1

    turnaround_times = []
    for i in range(len(job_queue)):
        turnaround_times.append(end_times[i] - job_queue[i][1][0])

    waiting_times = []
    for i in range(len(job_queue)):
        waiting_times.append(turnaround_times[i] - job_queue[i][1][1])

    avg_wt = 0
    for i in waiting_times:
        avg_wt += i
    avg_wt = (avg_wt / n)

    avg_tat = 0
    for i in turnaround_times:
        avg_tat += i
    avg_tat = (avg_tat / n)
    print("=================================================================================================")
    print("                                       SJF SCHEDULING                                            ")
    print("=================================================================================================")
    print("=================================================================================================")
    print("Process     |  Arrival  |   Burst  |   Start   |      End     | Turnaround (TAT) | Waiting (WT) |")
    print("=================================================================================================")

    for i in range(n):
        print("   ",
              str(job_queue[i][0]).rjust(4), "   |    ",
              str(job_queue[i][1][0]).rjust(4), " |   ",
              str(job_queue[i][1][1]).rjust(4), " |    ",
              str(start_times[i]).rjust(4), " |       ",
              str(end_times[i]).rjust(4), " |       ",
              str(turnaround_times[i]).rjust(4), "     |   ",
              str(waiting_times[i]).rjust(4), "     |  ")
        print("---------------------------------------------------------------------------------------------")

    print("   ", "Ave.", "   |           |          |           |              |     ", str(round(avg_tat, 4)).rjust(8),
          "   | ", str(round(avg_wt, 4)).rjust(8), "   |  ")
    print("=================================================================================================")


class SJFAlgorithm:
    pass
