import copy


def process(d, n, q):
    d = sorted(d.items(), key=lambda item: (item[1][0], item[1][1]))
    start_times = dict()
    end_times = dict()

    job_timeline = dict()
    job_queue = []
    work = d.copy()
    curr_start_time = d[0][1][0]
    i = 1
    while len(work) > 0 or curr_start_time == 100:
        filtered = filter(lambda item: curr_start_time >= item[1][0], work)
        filtered = sorted(filtered, key=lambda item: (item[1][3]))

        job = filtered.pop(0)

        service_time = q
        if job[1][1] < q:
            service_time = job[1][1]

        if job[0] in job_timeline.keys():
            job_timeline[job[0]] += service_time
        else:
            job_timeline[job[0]] = service_time
            job_queue.append(copy.deepcopy(job))
            start_times[job[0]] = curr_start_time

        job[1][1] -= service_time
        if job[1][1] == 0:
            work.remove(job)
            end_times[job[0]] = curr_start_time + service_time

        curr_start_time += service_time
        job[1][3] = curr_start_time + 1
        i += 1


    turnaround_times = dict()
    for i in range(len(job_queue)):
        turnaround_times[job_queue[i][0]] = (end_times[job_queue[i][0]] - job_queue[i][1][0])

    waiting_times = dict()
    for i in range(len(job_queue)):
        waiting_times[job_queue[i][0]] = (turnaround_times[job_queue[i][0]] - job_queue[i][1][1])

    avg_wt = 0
    for i in range(len(waiting_times)):
        avg_wt += list(waiting_times.values())[i]
    avg_wt = (avg_wt / n)

    avg_tat = 0
    for i in range(len(turnaround_times)):
        avg_tat += list(turnaround_times.values())[i]
    avg_tat = (avg_tat / n)
    print("=================================================================================================")
    print("                                        RR SCHEDULING                                            ")
    print("=================================================================================================")
    print("=================================================================================================")
    print("Process     |  Arrival  |   Burst  |   Start   |      End     | Turnaround (TAT) | Waiting (WT) |")
    print("=================================================================================================")

    for i in range(n):
        print("   ",
              str(job_queue[i][0]).rjust(4), "   |    ",
              str(job_queue[i][1][0]).rjust(4), " |   ",
              str(job_queue[i][1][1]).rjust(4), " |    ",
              str(start_times[job_queue[i][0]]).rjust(4), " |       ",
              str(end_times[job_queue[i][0]]).rjust(4), " |       ",
              str(turnaround_times[job_queue[i][0]]).rjust(4), "     |   ",
              str(waiting_times[job_queue[i][0]]).rjust(4), "     |  ")
        print("---------------------------------------------------------------------------------------------")

    print("   ", "Ave.", "   |           |          |           |              |     ",
          str(round(avg_tat, 4)).rjust(8),
          "   | ", str(round(avg_wt, 4)).rjust(8), "   |  ")
    print("=================================================================================================")


class RRAlgorithm:
    pass
